

;(function( namespace, undefined ){

    

    var Routing = can.Control({

        'route': function( data ){
            //cargar pagina 1
            can.route( ":type" );
            var url = can.route.url({  type: 'login'}, false );
            
            $(  App.config.mainContainer ).html( '<a href="' + url + '"> Entra </a>' );   


            /*testing


             nota: no se puede poner comodin entre las variables ejem:
             group/:id1/_@_:id2


              resultado deseado:
             url = "group/_@_migrupo/_@_mievento route";
             */
            var REG = {
                isRoute: /.route$/
            }
            var url = "group/tatata/tatata/tatata/ancla/tatata/tatta/migrupo/mievento";
            var comodin = "_@_";
            var ruta = "group/_@_/ancla/_@_/:groupname/:eventid route ".trim();

            var rutaSegments = ruta.split( comodin );
            var urlSe

            //contar cantidad de :ids dentro de la ruta
            var isRoute =  REG.isRoute.test(ruta);
            var rutaIdsCount = ruta.split( ':' ).length - 1 ;
            var rutaRaizSegment = ruta.split( ':' )[0];



            var test1 ="group/--/--/--/ancla/--/--/ancla/--/--/mariachi/parrot";
            var test2 = "group/_@_/ancla/_@_/ancla/_@_/:grupid/:eventid";
            var patt1 = /(group\/)(.+)(\/ancla\/)(.+)(\/ancla\/)(.+)(\/)([^\/]*)(\/)([^\/]*)/;
            var res1 = patt1.exec(test1);

            //console.log( test1 );
            console.log( this.comodin( test1, test2 ) );
            //console.log(  res1 );


        },


        'pokemon route': function( data ){
            console.log( data );
        },

        'group/_@_/:id route': function( data ) {
            console.log( 'method triggered' ); 
            console.log( data );
        },

        'group/_@_/moco/_@_/:gruponame/:event route': function( data ) {
            console.log( 'hola' );
        }

        
        
    });
    
    namespace.App = namespace.App || {};
    namespace.App.Route = Routing;
    
})( this );