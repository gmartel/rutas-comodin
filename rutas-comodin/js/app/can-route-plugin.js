	
    can.routeTools = can.routeTools || {};

    can.extend(can.routeTools, { 

		preCheckRoute = function( methodName ) {
			var isRoute =  /.route$/.test(methodName);
			methodName = methodName.trim();
			if( isRoute ) {
				return this.evaluateRouteWithComodin( location.hash.replace( /^#!/, ''), methodName.replace( /route$/, '') )
			}
			return methodName;
		},

        evaluateRouteWithComodin =  function( url, ruta, comodin = '_@_' ){

            var rutaIdsCount = ruta.split( ':' ).length - 1 ;
            var rutaBobySegment = ruta.split( ':' )[0]; //without :id/:id...etc
            var rutaSegments = rutaBobySegment.replace( /(\/)$/ , '').split( '/' );
            var strBodyReg = "";
            var strIdReg = ""
            var pattern;
            var comparison;
            var comodinPositions = [];

            //get RegGex for body
            for ( var i = 0; i < rutaSegments.length ; i++ ) {

                if( rutaSegments[ i ] !== comodin ) {
                    if( i === 0) {
                        rutaSegments[ i ] = '(' + rutaSegments[ i ] + '\/)';
                    } else {
                        rutaSegments[ i ] = '(\/' + rutaSegments[ i ] + '\/)';
                    }
                } else {
                    rutaSegments[ i ] = '(.+)';
                    comodinPositions.push( i + 1 );
                }
            }

            strBodyReg = rutaSegments.join('');

            //get regGex for ids
            for ( var i = 0; i < rutaIdsCount; i++ ) {
                    strIdReg +=  '(\/)([^\/]*)';
            }

            //get the general regex pattern
            pattern = RegExp( strBodyReg + strIdReg );

            //check if the url match the rute
            comparison = pattern.exec( url );


            //if the url match the route, 
            //insert the comodin in the url and return it
            if ( comparison ) {
                var temp = '';
                for ( var i = 0; i < comodinPositions.length ; i++ ) {
                    comparison[ comodinPositions[i] ] = comodin;
                }
                for ( var i = 1; i< comparison.length ; i++ ) {
                        temp += comparison[ i ];
                }

                //return new url
                return temp + '  route';
            }

            //if the url doesn't match the route 
            //return the same url
            return url;

        },