
;(function( namespace,undefined ){
    
    var form = can.Control({
        init: function( element, options ){
            var view = can.view( 'js/app/views/LoginForm/formulario.ejs', {} );
            $(  this.element ).html(  view );
            
            
            var model = can.Model({
                findOne: 'POST /usuario/{username}&{password}'
            }, {});
            
            can.fixture( 'POST /usuario/{username}&{password}', function( original, respondWidth, settings ){
                
                var success = false,
                    username = 'guillermo',
                    password = '123';
                    
                if ( original.data.username == username && original.data.password == password ) {
                    success = true;
                }
                
                respondWidth({
                    success: success
                });
                console.log( 'fixture findOne called');
                console.log( original );
            });
            
            can.fixture( 'js/app/views/LoginForm/formulario.ejs', 'js/app/views/LoginForm/formulario.ejs');
            
            this.model = model;
            
            
        },
        'button[id=btnLogin] click': function( obj, evt ){
            var username = $( this.element.selector + ' #inpUsername' ).val() ;
            var password = $( this.element.selector + ' #inpPassword' ).val() ;
            var self = this;
            this.model.findOne( {
                    username: username == '' ? '.': username,
                    password: password == '' ? '.': password
                }, function( model ){
                console.log( model.success );

                if ( model.success ) {
                    /**
                     * cambiar la ruta,
                     * luego escuchar esa ruta en route,
                     * crear la lista de perritos
                     */
                    can.route( ":type" );
                    can.route.attr({ type: 'listaPerritos'});
                    //new namespace.App.controllers.PerritosLista( self.element ) ;
                    self.destroy();
                } 

                
            });
        }
        
    });
    
    namespace.App = namespace.App || {};
    namespace.App.controllers = namespace.App.controllers || {};
    namespace.App.controllers.LoginForm = form;
    
    
    
})( this );