
;(function( namespace, undefined ){
    
    var lista = can.Control({
        init: function( element, options ){
            
            
            var perritosModel = can.Model({
                findAll: 'GET /getPerritos'
            }, {});
            
            can.fixture( 'GET /getPerritos', 'fixtures/listaPerritos/listaPerritos.json' );
            
            var respondData = null;
            perritosModel.findAll( {}, function( data ) {
                alert( data[0].name );
                respondData = data;
               var view = can.view( 'js/app/views/ListaPerritos/lista.ejs', data  )
               .then(
                        function( respond){ $( element ).html( view ); }
                );
               
            });
            
            

            
            
        }
    });
    
    namespace.App = namespace.App || {};
    namespace.App.controllers = namespace.App.controllers || {};
    namespace.App.controllers.PerritosLista = lista;
        
})( this );