	/**
    
    autor: Guillermo Martel
    email: guillermo.martel@laureate.net
    company: Laureate

    #### can-route-plugin.js ######

    
     This pugin upgrade can with the purpose to enable the use of wildcards on the declaration of the route actions of the controllers

     include this after canjs

     but, this puglin needs to override a private variable delcared in can, and for now i could not have found the a way to perform this 
     automatically.

     so, override manually your can.js file with this, this is in // ## can/route/route.js



         setState = can.route.setState = function() {
            var hash = can.route._getHash();
            var pivoteHash = "";


            //===================================   CAMBIOS LAUREATE =============================================================================================
            pivoteHash = can.routeTools.preCheckRoute( can.routeTools.routeMethod, hash , true ).replace( /route/, '').trim();
            
            if( pivoteHash !== hash ){
                //can.routeTools.routeController.papi = function(){ 'hello'; };
                curParams = can.route.deparam( can.routeTools.routeMethod );
                can.routeTools.routeController[ can.routeTools.routeMethod ](curParams);
            }
            // ----------  FIN CAMBIOS LAUREATE ---------

            curParams = can.route.deparam( hash );
            location.hash = '#!' + hash;
            
            
            // if the hash data is currently changing, or
            // the hash is what we set it to anyway, do NOT change the hash
            if(!changingData || hash !== lastHash){
                can.route.attr(curParams, true); location.hash = '#!' + hash;
            }
        }



    #####  USAGE #########
    
    _@_ : this is the wildcard
    you can add it where you want but before of any variable :id,
    and between a slash /,  like this /_@_/

    examples defining a route on a controller:

    1. 'group/_@_/:id'

    2. 'group/_@_/:name/:id'

    3. 'group/_@_/cake/_@_/:name/:id'

    4. '_@_/cake/:name/:Id'
       
    matched urls

    1. group/carbon/pokemon/45
        group/carbon/45

    2. group/pokemon/name/34

    3. group/pokemon/cake/otropoke/otropoke/name/56

    4. pokemon/charizard/cake/name/55


    */
    




    can.routeTools = can.routeTools || {};

    can.extend(can.routeTools, { 

        //=========================================================================
        //=====================   PROPERTIES   ====================================

        /**
        * method (route) that match the location.hash
        */
        routeMethod: "",

        /**
        * reference to the controller that is owner of the routeMethod
        */
        routeController: "",




        //=========================================================================
        //====================  METHODS ===========================================


        /**
        * If method match the location.hash then,
        * set the values of this.routeMethod and this.routeController  equals to method and controller
        * and returns true.
        * If not match, return false.
        * @method {string} name of the controller method 
        * @hash {string} the location.hash
        * @controller {can.controller} the controller owner of method
        * @isRoute {bool} true if the method has the action route in it. false if it doesn't. example : 'oranges route' => true
        * 
        */
        matchRouteMethod: function( method, hash, controller, isRoute = undefined ){

            var tempUrl = this.preCheckRoute( method, hash, isRoute );
            tempUrl = tempUrl.trim();
            hash = hash.replace( /^#!/, '');
            if ( tempUrl !== hash ) { 
                this.routeMethod = method;  
                this.routeController = controller;
                return true;
            }

            return false;

        },

        setObserve: function(){
            var obs = can.Observe( location.hash );
            //can.route.data
            obs.bind('change', function(ev, attr, how, newVal, oldVal) {
                console.log(' location has change.');
            });
        },

        /**
        *   checks if the methodName if an action type route, 
        *   if is true, checks if the url has match this action,
        *   if it is true, reformat the url to a valid url format
        *   format of the methodName = name/submane route
        *   or methodName = name/submane + isRoute = true
        * @param string methodName name of the method of a can.controller
        * @param string url the same in location.hash
          @param bool isRoute true if the method is a route, if its unkonw just don't add this param.
        * @return string url in a valid format if neccessary
        */
		preCheckRoute: function( methodName , url, isRoute = undefined ) {
			if ( isRoute === undefined  ) {  isRoute =  /.route$/.test(methodName); }
			methodName = methodName.trim();
			if( isRoute ) {
				//return this.evaluateRouteWithComodin( location.hash.replace( /^#!/, ''), methodName.replace( /route$/, '') );
                return this.evaluateRouteWithComodin( url.replace( /^#!/, ''), methodName.replace( /route$/, '') );
			}
			return url;
		},

        /**
        * Evaluate if a route has a comodin in it,
        * if it has, convert the route to a valid format.
        * @example:
        *   ruta = group/_@_/:id , url = group/tejones/pokemon/azul
        *   new url after be formatted = group/_@_/azul
        * @param string url the url in the location.hash : group/tejones/22
        * @param string ruta the route to be match,group/_@_/:id, this is used by can.route to match the routes
        * @return string the formatted url = group/_@_/22 
        */
        evaluateRouteWithComodin: function( url, ruta, comodin = '_@_' ){

            var rutaIdsCount = ruta.split( ':' ).length - 1 ;
            var rutaBobySegment = ruta.split( ':' )[0]; //without :id/:id...etc
            var rutaSegments = rutaBobySegment.replace( /(\/)$/ , '').split( '/' );
            var strBodyReg = "";
            var strIdReg = ""
            var pattern;
            var comparison;
            var comodinPositions = [];

            //get RegGex for body
            for ( var i = 0; i < rutaSegments.length ; i++ ) {

                if( rutaSegments[ i ] !== comodin ) {
                    if( i === 0) {
                        rutaSegments[ i ] = '(' + rutaSegments[ i ] + '\/)';
                    } else {
                        rutaSegments[ i ] = '(\/' + rutaSegments[ i ] + '\/)';
                    }
                } else {
                    rutaSegments[ i ] = '(.+)';
                    comodinPositions.push( i + 1 );
                }
            }

            strBodyReg = rutaSegments.join('');

            //get regGex for ids
            for ( var i = 0; i < rutaIdsCount; i++ ) {
                    strIdReg +=  '(\/)([^\/]*)';
            }

            //get the general regex pattern
            pattern = RegExp( strBodyReg + strIdReg );

            //check if the url match the rute
            comparison = pattern.exec( url );


            //if the url match the route, 
            //insert the comodin in the url and return it
            if ( comparison ) {
                var temp = '';
                for ( var i = 0; i < comodinPositions.length ; i++ ) {
                    comparison[ comodinPositions[i] ] = comodin;
                }
                for ( var i = 1; i< comparison.length ; i++ ) {
                        temp += comparison[ i ];
                }

                //return new url
                return temp + '  route';
            }

            //if the url doesn't match the route 
            //return the same url
            return url;

        },

        test: function(){
            console.log( 'test original' );
        }

    });


    can.Control.processors.route = function( el, event, selector, funcName, controller ) {

        //==== CAMBIOS LAUREATE ======
        can.routeTools.matchRouteMethod( funcName.trim(), location.hash, controller, true );
        //---- FIN CAMBIOS LAUREATE -----

        selector = selector || "";
        can.route( selector );
        var batchNum,
            check = function( ev, attr, how ) { 
                if ( can.route.attr('route') === ( selector ) && 
                    ( ev.batchNum === undefined || ev.batchNum !== batchNum ) ) {
                    
                    batchNum = ev.batchNum;
                    
                    var d = can.route.attr();
                    delete d.route;
                    if ( can.isFunction( controller[ funcName ] )) {
                        controller[funcName]( d );
                    } else {
                        controller[controller[funcName]](d);
                    }
                    
                }
            };
        can.route.bind( 'change', check );
        return function() {
            can.route.unbind( 'change', check );
        };
    };